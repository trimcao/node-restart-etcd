/*
Package main implements an etcd parsing library.
For now it is used specifically for node restart script.
*/
package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type cxp struct {
	name   string
	ID     string
	region string
}

type az struct {
	name string
}

type tunnel struct {
	ID         string
	sourceType string
	sourceName string
	destType   string
	destName   string
	tunnelNo   string
	tenantID   string
	cxp        string
	az         string
}

type bnode struct {
	name            string
	ens0IP          string
	ens1IP          string
	ens2IP          string
	ens3IP          string
	mac             string
	ingressTunnelID string
	snodeTunnelID   string
	rrTunnelID      string
	cxp             string
	az              string
}

type ingress struct {
	name   string
	ens0IP string
	ens1IP string
	ens2IP string
	cxp    string
	az     string
}

type snode struct {
	name          string
	ens0IP        string
	ens1IP        string
	ens2IP        string
	ens3IP        string
	ens4IP        string
	ens5IP        string
	ens6IP        string
	mac           string
	bnodeTunnelID string
	vnodeTunnelID string
	rrTunnelID    string
	pantTunnelID  string
	panuTunnelID  string
	cxp           string
	az            string
}

type vnode struct {
	name            string
	ens0IP          string
	ens1IP          string
	ens2IP          string
	ens3IP          string
	ens2PublicIP    string
	mac             string
	egress0TunnelID string
	egress1TunnelID string
	snodeTunnelID   string
	rrTunnelID      string
	cxp             string
	az              string
}

type rrnode struct {
	name          string
	ens0IP        string
	ens1IP        string
	mac           string
	bnodeTunnelID string
	snodeTunnelID string
	vnodeTunnelID string
	cxp           string
	az            string
}

type pannode struct {
	name   string
	ens1IP string
	ens2IP string
	cxp    string
	az     string
}

type egress struct {
	name                  string
	remote0IP             string
	remote1IP             string
	secret0               string
	secret1               string
	localTunnelInsideIP0  string
	localTunnelInsideIP1  string
	remoteTunnelInsideIP0 string
	remoteTunnelInsideIP1 string
	localAsn              string
	remoteAsn             string
	cxp                   string
	az                    string
}

type tenant struct {
	tenantID  string
	tunnelIDs []string
	bnode     string
	snode     string
	vnode     string
	rrnode    string
	pannode   string
	ingress   string
	egress    string
	cxp       string
	az        string
}

func main() {
	resp, err := http.Get("http://10.1.8.92:2379/v2/keys/?recursive=true")
	// plan, _ := ioutil.ReadFile("etcd.json")

	var result map[string]interface{}
	// we will use the following if we obtain the json from http request
	json.NewDecoder(resp.Body).Decode(&result)
	// if work with file
	// err := json.Unmarshal(plan, &result)
	if err != nil {
		panic(err)
	}

	m := iterateJSON(result)

	// parse the etcd data
	etcdNodes, etcdTunnels, etcdCXPs := parseEtcd(m)
	updateTunnelFields(etcdNodes, etcdTunnels)
	getTenantExperiment := true
	tenants := getTenants(etcdTunnels, getTenantExperiment)
	// print out the node data
	for name, node := range etcdNodes {
		log.Println(name, node)
	}
	for name, tunnel := range etcdTunnels {
		log.Println(name, tunnel)
	}
	for name, cxp := range etcdCXPs {
		log.Println(name, cxp)
	}
	for name, tenant := range tenants {
		log.Println(name, tenant)
	}

	extraVars := getVars(tenants, etcdNodes, etcdCXPs)

	// run ansible playbook for each tenant
	for tenantID := range tenants {
		if tenantID == "tenant1" {
			vars := extraVars[tenantID]
			var varSlice []string
			for key, val := range vars {
				varSlice = append(varSlice, fmt.Sprintf("%s=%s", key, val))
			}
			tenantVars := strings.Join(varSlice, " ")
			defaultVars := "api_skip=true"
			allVars := fmt.Sprintf("%s %s", defaultVars, tenantVars)
			command := "ansible-playbook"
			args := []string{"/home/admin/tenant/node_restart_vxlan.yml", "--extra-vars", allVars}
			log.Println(allVars)
			cmd := exec.Command(command, args...)
			cmd.Dir = "/home/admin/tenant"
			cmd.Env = os.Environ()
			// try real-time output
			stdout, _ := cmd.StdoutPipe()
			cmd.Start()
			oneByte := make([]byte, 500)
			// num := 1
			for {
				_, err := stdout.Read(oneByte)
				if err != nil {
					log.Printf(err.Error())
					break
				}
				r := bufio.NewReader(stdout)
				line, _, _ := r.ReadLine()
				log.Println(string(line))
				// num = num + 1
				// if num > 3 {
				// 	os.Exit(0)
				// }
			}

			// var out bytes.Buffer
			// cmd.Stdout = &out
			// log.Println(out.String())
			cmd.Wait()
		}
	}
}

// iterateJSON parses etcd in JSON format into a map of key-value pairs.
// All the directories are discarded. Only key-value pairs are kept.
func iterateJSON(input interface{}) map[string]string {
	parsedData := make(map[string]string)
	iterateJSONHelper(input, parsedData)
	return parsedData
}

// iterateJSONHelper is a helper recursive method for iterateJSON
func iterateJSONHelper(x interface{}, m map[string]string) {
	switch t := x.(type) {
	case map[string]interface{}:
		mapData := x.(map[string]interface{})
		for key, val := range mapData {
			if key == "value" {
				m[mapData["key"].(string)] = val.(string)
			} else {
				iterateJSONHelper(val, m)
			}
		}
	case string:
	case []interface{}:
		for _, val := range x.([]interface{}) {
			iterateJSONHelper(val, m)
		}
	case bool:
	case float64:
	default:
		log.Println(t, x)
		log.Printf("%T\n", x)
		log.Println("unknown")
	}
}

// parseEtcd gets the nodes and tunnels data from key-value pairs
func parseEtcd(data map[string]string) (map[string]interface{}, map[string]*tunnel, map[string]*cxp) {
	const LenNodeData = 8
	const LenEgressData = 9
	const LenTunnelStatement = 12
	const LenVnodeEgressTunnel = 13
	const LenCXPData = 4
	const LenAZData = 6

	nodes := make(map[string]interface{})
	cxps := make(map[string]*cxp)
	tunnels := make(map[string]*tunnel)
	for path, value := range data {
		comp := strings.Split(path, "/")
		if len(comp) == LenNodeData {
			cxpName := comp[2]
			azName := comp[4]
			nodeType := comp[5]
			nodeName := comp[6]
			entry := comp[7]
			switch nodeType {
			case "BNODE":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &bnode{name: nodeName}
				}
				node := nodes[nodeName].(*bnode)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens0_ip":
					node.ens0IP = value
				case "ens1_ip":
					node.ens1IP = value
				case "ens2_ip":
					node.ens2IP = value
				case "ens3_ip":
					node.ens3IP = value
				case "mac":
					node.mac = value
				}
			case "SNODE":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &snode{name: nodeName}
				}
				node := nodes[nodeName].(*snode)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens0_ip":
					node.ens0IP = value
				case "ens1_ip":
					node.ens1IP = value
				case "ens2_ip":
					node.ens2IP = value
				case "ens3_ip":
					node.ens3IP = value
				case "ens4_ip":
					node.ens4IP = value
				case "ens5_ip":
					node.ens5IP = value
				case "ens6_ip":
					node.ens6IP = value
				case "mac":
					node.mac = value
				}
			case "INGRESS":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &ingress{name: nodeName}
				}
				node := nodes[nodeName].(*ingress)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens0_ip":
					node.ens0IP = value
				case "ens1_ip":
					node.ens1IP = value
				case "ens2_ip":
					node.ens2IP = value
				}
			case "PAN":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &pannode{name: nodeName}
				}
				node := nodes[nodeName].(*pannode)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens1_ip":
					node.ens1IP = value
				case "ens2_ip":
					node.ens2IP = value
				}
			case "VNODE":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &vnode{name: nodeName}
				}
				node := nodes[nodeName].(*vnode)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens0_ip":
					node.ens0IP = value
				case "ens1_ip":
					node.ens1IP = value
				case "ens2_ip":
					node.ens2IP = value
				case "ens3_ip":
					node.ens3IP = value
				case "ens2_public_ip":
					node.ens2PublicIP = value
				case "mac":
					node.mac = value
				}
			case "RR":
				if _, ok := nodes[nodeName]; !ok {
					nodes[nodeName] = &rrnode{name: nodeName}
				}
				node := nodes[nodeName].(*rrnode)
				node.cxp = cxpName
				node.az = azName
				switch entry {
				case "ens0_ip":
					node.ens0IP = value
				case "ens1_ip":
					node.ens1IP = value
				case "mac":
					node.mac = value
				}
			}
		} else if len(comp) == LenEgressData || len(comp) == LenEgressData+1 {
			cxpName := comp[2]
			azName := comp[4]
			nodeType := comp[5]
			if nodeType != "EGRESS" {
				continue
			}
			nodeName := comp[6]
			if _, ok := nodes[nodeName]; !ok {
				nodes[nodeName] = &egress{name: nodeName}
			}
			node := nodes[nodeName].(*egress)
			node.cxp = cxpName
			node.az = azName
			tunnelNo := comp[7]
			if len(comp) == LenEgressData {
				entry := comp[8]
				if entry == "remote_ip" {
					if tunnelNo == "tunnel0" {
						node.remote0IP = value
					} else if tunnelNo == "tunnel1" {
						node.remote1IP = value
					}
				} else if entry == "local_asn" {
					node.localAsn = value
				} else if entry == "remote_asn" {
					node.remoteAsn = value
				} else if entry == "local_tunnel_inside_ip" {
					if tunnelNo == "tunnel0" {
						node.localTunnelInsideIP0 = value
					} else if tunnelNo == "tunnel1" {
						node.localTunnelInsideIP1 = value
					}
				} else if entry == "remote_tunnel_inside_ip" {
					if tunnelNo == "tunnel0" {
						node.remoteTunnelInsideIP0 = value
					} else if tunnelNo == "tunnel1" {
						node.remoteTunnelInsideIP1 = value
					}
				}
			} else {
				entry := comp[9]
				if entry == "pre_shared_key" {
					if tunnelNo == "tunnel0" {
						node.secret0 = value
					} else if tunnelNo == "tunnel1" {
						node.secret1 = value
					}
				}
			}
		} else if len(comp) == LenTunnelStatement || len(comp) == LenVnodeEgressTunnel {
			cxpName := comp[2]
			azName := comp[4]
			sourceType := comp[5]
			sourceName := comp[6]
			tenantName := comp[7]
			destType := comp[9]
			destName := comp[10]
			key := comp[len(comp)-1]
			if key != "tunnel_id" {
				continue
			}
			tunnelID := value
			if _, ok := tunnels[tunnelID]; ok {
				continue
			}
			tunnels[tunnelID] = &tunnel{ID: tunnelID, sourceType: sourceType, sourceName: sourceName,
				destType: destType, destName: destName, tenantID: tenantName, cxp: cxpName, az: azName}
			if len(comp) == LenVnodeEgressTunnel {
				tunnelNo := comp[11]
				tunnels[tunnelID].tunnelNo = tunnelNo
			}
		} else if len(comp) == LenCXPData {
			cxpName := comp[2]
			entry := comp[3]
			if _, ok := cxps[cxpName]; !ok {
				cxps[cxpName] = &cxp{name: cxpName}
			}
			cxp := cxps[cxpName]
			switch entry {
			case "cxp_region":
				cxp.region = value
			case "cxp_id":
				cxp.ID = value
			}
		}
	}
	return nodes, tunnels, cxps
}

// updateTunnelFields fills the tunnel fields for the nodes.
// For example, a SNODE may have a SNODE-BNODE tunnel, and this function will find
// the correct tunnel id and put that into the node parameter.
func updateTunnelFields(nodes map[string]interface{}, tunnels map[string]*tunnel) {
	for tunnelID, tunnel := range tunnels {
		if tunnel.sourceType == "BNODE" || tunnel.destType == "BNODE" {
			var bnodeName string
			if tunnel.sourceType == "BNODE" {
				bnodeName = tunnel.sourceName
			} else {
				bnodeName = tunnel.destName
			}
			bnode := nodes[bnodeName].(*bnode)
			if tunnel.sourceType == "SNODE" || tunnel.destType == "SNODE" {
				bnode.snodeTunnelID = tunnelID
			} else if tunnel.sourceType == "INGRESS" || tunnel.destType == "INGRESS" {
				bnode.ingressTunnelID = tunnelID
			} else if tunnel.sourceType == "RR" || tunnel.destType == "RR" {
				bnode.rrTunnelID = tunnelID
			}
		}
		if tunnel.sourceType == "SNODE" || tunnel.destType == "SNODE" {
			var snodeName string
			if tunnel.sourceType == "SNODE" {
				snodeName = tunnel.sourceName
			} else {
				snodeName = tunnel.destName
			}
			snode := nodes[snodeName].(*snode)
			if tunnel.sourceType == "BNODE" || tunnel.destType == "BNODE" {
				snode.bnodeTunnelID = tunnelID
			} else if tunnel.sourceType == "VNODE" || tunnel.destType == "VNODE" {
				snode.vnodeTunnelID = tunnelID
			} else if tunnel.sourceType == "RR" || tunnel.destType == "RR" {
				snode.rrTunnelID = tunnelID
			} else if tunnel.sourceType == "PANU" || tunnel.destType == "PANU" {
				snode.panuTunnelID = tunnelID
			} else if tunnel.sourceType == "PANT" || tunnel.destType == "PANT" {
				snode.pantTunnelID = tunnelID
			}
		}
		if tunnel.sourceType == "VNODE" || tunnel.destType == "VNODE" {
			var vnodeName string
			if tunnel.sourceType == "VNODE" {
				vnodeName = tunnel.sourceName
			} else {
				vnodeName = tunnel.destName
			}
			vnode := nodes[vnodeName].(*vnode)
			if tunnel.sourceType == "EGRESS" || tunnel.destType == "EGRESS" {
				if tunnel.tunnelNo == "tunnel0" {
					vnode.egress0TunnelID = tunnelID
				} else if tunnel.tunnelNo == "tunnel1" {
					vnode.egress1TunnelID = tunnelID
				}
			} else if tunnel.sourceType == "SNODE" || tunnel.destType == "SNODE" {
				vnode.snodeTunnelID = tunnelID
			} else if tunnel.sourceType == "RR" || tunnel.destType == "RR" {
				vnode.rrTunnelID = tunnelID
			}
		}
		if tunnel.sourceType == "RR" || tunnel.destType == "RR" {
			var rrnodeName string
			if tunnel.sourceType == "RR" {
				rrnodeName = tunnel.sourceName
			} else {
				rrnodeName = tunnel.destName
			}
			rrnode := nodes[rrnodeName].(*rrnode)
			if tunnel.sourceType == "SNODE" || tunnel.destType == "SNODE" {
				rrnode.snodeTunnelID = tunnelID
			} else if tunnel.sourceType == "BNODE" || tunnel.destType == "BNODE" {
				rrnode.bnodeTunnelID = tunnelID
			} else if tunnel.sourceType == "VNODE" || tunnel.destType == "VNODE" {
				rrnode.vnodeTunnelID = tunnelID
			}
		}
	}
}

// getTenants reads the tunnels and gather tenants data.
// A tenant represents tenant_variables file used in ansible playbooks.
func getTenants(tunnels map[string]*tunnel, experiment bool) map[string]*tenant {
	tenants := make(map[string]*tenant)
	for tunnelID, tunnel := range tunnels {
		tenantID := tunnel.tenantID
		// TODO: tenantID is not always "tenant1"
		if experiment {
			tenantID = "tenant1"
		}
		if _, ok := tenants[tenantID]; !ok {
			tenants[tenantID] = &tenant{tenantID: tenantID}
		}
		tenant := tenants[tenantID]
		tenant.tunnelIDs = append(tenant.tunnelIDs, tunnelID)
		sourceType := tunnel.sourceType
		sourceName := tunnel.sourceName
		destType := tunnel.destType
		destName := tunnel.destName
		switch sourceType {
		case "BNODE":
			tenant.bnode = sourceName
		case "SNODE":
			tenant.snode = sourceName
		case "VNODE":
			tenant.vnode = sourceName
		case "RR":
			tenant.rrnode = sourceName
		}
		switch destType {
		case "BNODE":
			tenant.bnode = destName
		case "SNODE":
			tenant.snode = destName
		case "VNODE":
			tenant.vnode = destName
		case "PANU":
			tenant.pannode = destName
		case "PANT":
			tenant.pannode = destName
		case "RR":
			tenant.rrnode = destName
		case "INGRESS":
			tenant.ingress = destName
		case "EGRESS":
			tenant.egress = destName
		}
	}
	return tenants
}

// getVars gather the variables required for node_restart playbook for each tenant
func getVars(tenants map[string]*tenant, nodes map[string]interface{}, cxps map[string]*cxp) map[string]map[string]string {
	// Obtain vars for each tenant
	vars := make(map[string]map[string]string)
	for tenantID, tenant := range tenants {
		vars[tenantID] = make(map[string]string)
		tenantVars := vars[tenantID]
		tenantVars["tenant_id"] = tenant.tenantID
		// include the environment vars
		cxpName := os.Getenv("CXP")
		cxp := cxps[cxpName]
		tenantVars["node_type"] = os.Getenv("NODE_TYPE")
		tenantVars["node_name"] = os.Getenv("NODE_NAME")
		tenantVars["gateway"] = os.Getenv("GATEWAY")
		tenantVars["cxp_name"] = cxp.name
		tenantVars["cxp_region"] = cxp.region

		if bnodeInstance, ok := nodes[tenant.bnode]; ok {
			bnode := bnodeInstance.(*bnode)
			tenantVars["bnode"] = tenant.bnode
			tenantVars["bnode_ens0_ip"] = bnode.ens0IP
			tenantVars["bnode_ens1_ip"] = bnode.ens1IP
			tenantVars["bnode_ens2_ip"] = bnode.ens2IP
			tenantVars["bnode_ens3_ip"] = bnode.ens3IP
			tenantVars["bnode_mac"] = bnode.mac
			tenantVars["bnode_ingress_tunnel_id"] = bnode.ingressTunnelID
			tenantVars["bnode_rr_tunnel_id"] = bnode.rrTunnelID
			rrLoopback, err := strconv.Atoi(bnode.rrTunnelID)
			if err != nil {
				panic(err)
			}
			tenantVars["bnode_rr_loopback"] = strconv.Itoa(rrLoopback + 1000)
		}

		if snodeInstance, ok := nodes[tenant.snode]; ok {
			snode := snodeInstance.(*snode)
			tenantVars["snode"] = tenant.snode
			tenantVars["snode_ens0_ip"] = snode.ens0IP
			tenantVars["snode_ens1_ip"] = snode.ens1IP
			tenantVars["snode_ens2_ip"] = snode.ens2IP
			tenantVars["snode_ens3_ip"] = snode.ens3IP
			tenantVars["snode_ens4_ip"] = snode.ens4IP
			tenantVars["snode_ens5_ip"] = snode.ens5IP
			tenantVars["snode_ens6_ip"] = snode.ens6IP
			tenantVars["snode_mac"] = snode.mac
			tenantVars["snode_bnode_tunnel_id"] = snode.bnodeTunnelID
			tenantVars["snode_vnode_tunnel_id"] = snode.vnodeTunnelID
			tenantVars["snode_rr_tunnel_id"] = snode.rrTunnelID
			tenantVars["snode_pant_tunnel_id"] = snode.pantTunnelID
			tenantVars["snode_panu_tunnel_id"] = snode.panuTunnelID
			rrLoopback, err := strconv.Atoi(snode.rrTunnelID)
			if err != nil {
				panic(err)
			}
			tenantVars["snode_rr_loopback"] = strconv.Itoa(rrLoopback + 1000)

		}

		if vnodeInstance, ok := nodes[tenant.vnode]; ok {
			vnode := vnodeInstance.(*vnode)
			tenantVars["vnode"] = tenant.vnode
			tenantVars["vnode_ens0_ip"] = vnode.ens0IP
			tenantVars["vnode_ens1_ip"] = vnode.ens1IP
			tenantVars["vnode_ens2_ip"] = vnode.ens2IP
			tenantVars["vnode_ens3_ip"] = vnode.ens3IP
			tenantVars["vnode_ens2_public_ip"] = vnode.ens2PublicIP
			tenantVars["vnode_mac"] = vnode.mac
			tenantVars["vnode_egress_0_tunnel_id"] = vnode.egress0TunnelID
			tenantVars["vnode_egress_1_tunnel_id"] = vnode.egress1TunnelID
			tenantVars["vnode_snode_tunnel_id"] = vnode.snodeTunnelID
			tenantVars["vnode_rr_tunnel_id"] = vnode.rrTunnelID
			rrLoopback, err := strconv.Atoi(vnode.rrTunnelID)
			if err != nil {
				panic(err)
			}
			tenantVars["vnode_rr_loopback"] = strconv.Itoa(rrLoopback + 1000)
		}

		if rrnodeInstance, ok := nodes[tenant.rrnode]; ok {
			rrnode := rrnodeInstance.(*rrnode)
			tenantVars["rrnode"] = tenant.rrnode
			tenantVars["rr_ens0_ip"] = rrnode.ens0IP
			tenantVars["rr_ens1_ip"] = rrnode.ens1IP
			tenantVars["rr_mac"] = rrnode.mac
			tenantVars["rr_bnode_tunnel_id"] = rrnode.bnodeTunnelID
			tenantVars["rr_snode_tunnel_id"] = rrnode.snodeTunnelID
			tenantVars["rr_vnode_tunnel_id"] = rrnode.vnodeTunnelID
		}

		if ingressInstance, ok := nodes[tenant.ingress]; ok {
			ingress := ingressInstance.(*ingress)
			tenantVars["ingress"] = tenant.ingress
			tenantVars["ingress_ens2_ip"] = ingress.ens2IP
		}

		if egressInstance, ok := nodes[tenant.egress]; ok {
			egress := egressInstance.(*egress)
			tenantVars["egress"] = tenant.egress
			tenantVars["egress_remote0_ip"] = egress.remote0IP
			tenantVars["egress_remote1_ip"] = egress.remote1IP
			tenantVars["egress_tunnel0_secret"] = egress.secret0
			tenantVars["egress_tunnel1_secret"] = egress.secret1
			tenantVars["egress_local0_tunnel_inside_ip"] = egress.localTunnelInsideIP0
			tenantVars["egress_local1_tunnel_inside_ip"] = egress.localTunnelInsideIP1
			tenantVars["egress_remote0_tunnel_inside_ip"] = egress.remoteTunnelInsideIP0
			tenantVars["egress_remote1_tunnel_inside_ip"] = egress.remoteTunnelInsideIP1
			tenantVars["egress_local_asn"] = egress.localAsn
			tenantVars["egress_remote_asn"] = egress.remoteAsn
		}

		if pannodeInstance, ok := nodes[tenant.pannode]; ok {
			pannode := pannodeInstance.(*pannode)
			tenantVars["pannode"] = tenant.pannode
			tenantVars["pan_ens1_ip"] = pannode.ens1IP
			tenantVars["pan_ens2_ip"] = pannode.ens2IP
		}
	}
	return vars
}
